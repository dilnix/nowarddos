#!/bin/bash
run() {
    os_release=$(awk -F= '/^NAME/{print $2}' /etc/os-release)
    if [ "$os_release" == '"Ubuntu"' ]; then
        apt update -y
        apt upgrade -y
        apt install python3-pip nload -y
    elif [ "$os_release" == '"CentOS Linux"' ]; then
        yum update -y
        yum install python3-pip nload -y
    elif [ "$os_release" == '"Fedora Linux"' ]; then
        dnf update -y
        dnf install python3-pip nload -y
    else
        echo "Unsupported OS version"
        exit 1
    fi
    cp update.sh ..
    pip3 install -r requirements.txt

    # remove old crons for script
    crontab -u root -l | grep -v 'flood.sh restart'  | crontab -u root -
    crontab -u root -l | grep -v 'sh update.sh'  | crontab -u root -

    # re-install new crons
    restart_croncmd="cd $(pwd) && bash flood.sh restart"
    restart_cronjob="0,15,45 * * * * $restart_croncmd"
    ( crontab -l | grep -v -F "$restart_croncmd" ; echo "$restart_cronjob" ) | crontab -

    update_parent_dir=$(cd ../ && pwd)
    update_current_dir=$(pwd)
    update_croncmd="cd ${update_parent_dir} && bash update.sh ${update_current_dir}"
    update_cronjob="30 * * * * $update_croncmd"
    ( crontab -l | grep -v -F "$update_croncmd" ; echo "$update_cronjob" ) | crontab -

    rm -rf ../logs
    mkdir -p ../logs
    touch ../logs/nowar.log
    nohup python3 nowarddos.py > ../logs/nowar.log 2>&1 &

    echo ">>>

    Автоматизацію підключено. Скрипт буде перезавантажуватися кожні 15 хв, та оновлюватися - кожну годину.
    Подивитись логи: ./flood.sh log. Перелік інших команд: ./flood.sh ?

    Слава Україні! Ми переможемо!

    <<<"
}

status() {
    pid=$(ps -ax |grep 'python3 nowarddos.py' |grep -v grep |awk '{print $1}')
    if [ "$pid" ]; then
        echo "NoWar is running with PID: $pid. To view logs, run:
        ./flood.sh log"
    else
        echo "NoWar is not running. To start it, run:
        ./flood.sh run"
    fi
}

restart() {
    cp update.sh ..

    ps -ax | grep 'python3 nowarddos.py' | grep -v grep | awk '{print $1}' | xargs kill -9 &> /dev/null

    # fixme temporary line for old filename "main.py"
    ps -ax | grep 'python3 main.py' | grep -v grep | awk '{print $1}' | xargs kill -9 &> /dev/null

    rm -rf ../logs
    mkdir -p ../logs
    touch ../logs/nowar.log

    nohup python3 nowarddos.py > ../logs/nowar.log 2>&1 &
}

stop() {
    ps -ax | grep 'python3 nowarddos.py' | grep -v grep | awk '{print $1}' | xargs kill -9 &> /dev/null

    # fixme temporary line for old filename "main.py"
    ps -ax | grep 'python3 main.py' | grep -v grep | awk '{print $1}' | xargs kill -9 &> /dev/null

    rm -rf ../logs
}

log() {
    log_file="../logs/nowar.log"
    if [ -f "${log_file}" ]; then
        tail -f -n 100 ${log_file}
    else
        echo "There is no log file: ${log_file}. Is script currently running? To check, type:
        ./flood.sh status"
    fi
}

net() {
    nload eth0
}

case "$1" in
run)
    run "$@"; exit $?;;
status)
    status "$@"; exit $?;;
restart)
    restart "$@"; exit $?;;
stop)
    stop "$@"; exit $?;;
log)
    log "$@"; exit $?;;
net)
    net "$@"; exit $?;;
*)
    echo "Usage: $0
    run
    status
    restart
    log
    net
    stop";
    exit 1;
esac
exit 0