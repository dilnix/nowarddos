#!/bin/bash
cd "$1" # dir with python script

# check if update is required
server_version=$(curl -L -s https://gitlab.com/dilnix/nowarddos/-/raw/main/version.txt | head)
local_version=$(cat version.txt)

if [ "$server_version" = "$local_version" ]; then
    echo "No update is required. Installed latest version: $local_version"
    exit 1
fi

echo "Update is required. Server version is $server_version. Local version is $local_version"

cd "$( dirname "$1" )" # parent dir of python script
git clone "https://gitlab.com/dilnix/nowarddos.git" "tmp" || exit 1

ps -ax | grep 'python3 nowarddos.py' | grep -v grep | awk '{print $1}' | xargs kill -9 &> /dev/null

# fixme temporary line for old filename "main.py"
ps -ax | grep 'python3 main.py' | grep -v grep | awk '{print $1}' | xargs kill -9 &> /dev/null

rm -rf "$1"
mv "tmp" "$1"

rm -rf logs
mkdir -p logs
touch logs/nowar.log

cd "$1"

cp update.sh ..

# start python script directly
nohup python3 nowarddos.py > ../logs/nowar.log 2>&1